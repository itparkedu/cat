package com.example.cats.repository;

import com.example.cats.model.Cat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface CatReposirory extends JpaRepository<Cat, Long> {

    List<Cat> findAllByOrderByLoiseDesc();
}
