package com.example.cats.controller;


import com.example.cats.model.Cat;
import com.example.cats.model.Pair;
import com.example.cats.service.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cats")
public class CatController {

    private CatService service;

    @Autowired
    public CatController(CatService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('cats:read')")
    public Cat getById(@PathVariable Long id){
        return service.getById(id);
    }

    @GetMapping("/vote")
    @PreAuthorize("hasAuthority('cats:read')")
    public List<Pair> getVoteList(){
        return service.geVoteList();
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasAuthority('cats:write')")
    public Cat setLike(@PathVariable Long id){
        return service.incrementLike(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('cats:read')")
    public List<Cat> getFavoriteCatsByDesc(){
        return service.getWinner();
    }

}
