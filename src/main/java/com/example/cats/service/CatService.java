package com.example.cats.service;

import com.example.cats.model.Cat;
import com.example.cats.model.Pair;
import com.example.cats.model.User;
import com.example.cats.repository.CatReposirory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CatService {

    private CatReposirory reposirory;

    private Map<Session, User> sessionUserMap = new HashMap<>();

    @Autowired
    public CatService(CatReposirory reposirory) {
        this.reposirory = reposirory;
    }

    public Cat getById(Long id) {
        return reposirory.getById(id);
    }

    public List<Pair> geVoteList() {
        List<Cat> cats = reposirory.findAll();
        List<Pair> pairList = new ArrayList<>();
//        cats.stream().forEach(animal1 -> cats.stream().forEach(animal2 ->pairList.add(new Pair(animal1,animal2))));
        for (int i = 0; i < cats.size(); i++) {
            for (int j = i+1; j < cats.size(); j++) {
                pairList.add(new Pair(cats.get(i), cats.get(j)));
            }
        }
        return pairList;
    }

    public Cat incrementLike(Long id) {
        Cat cat = reposirory.getById(id);
        cat.setLoise(cat.getLoise() + 1);
        return reposirory.save(cat);
    }

    public List<Cat> getWinner() {
        return reposirory.findAllByOrderByLoiseDesc();
    }

}
