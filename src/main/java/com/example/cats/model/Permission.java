package com.example.cats.model;

public enum Permission {
    CATS_READ("cats:read"),
    CATS_WRITE("cats:write");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}