package com.example.cats.model;

public enum Status {
    ACTIVE, BANNED;
}
