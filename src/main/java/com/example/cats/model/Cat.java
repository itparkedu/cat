package com.example.cats.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "cats")
public class Cat {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    String name;

    @Column(name = "loise")
    Integer loise;

}
