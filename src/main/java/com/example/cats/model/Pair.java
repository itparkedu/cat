package com.example.cats.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Pair {
    Cat animal1;
    Cat animal2;
}
