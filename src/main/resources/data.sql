DELETE FROM users;
DELETE FROM cats;

INSERT INTO users (id, email, first_name, last_name, password, role, status)
VALUES  (1, 'user0@mail.ru', 'alex', 'ivanov', '$2a$12$sru0m7XHmeFXzFHYnL5cwuumWy1/K0CE.rXPqcUTjl8ftQWyngnMK', 'USER', 'ACTIVE'),
        (2, 'user1@mail.ru', 'bob', 'ivanov', ' $2a$12$sru0m7XHmeFXzFHYnL5cwuumWy1/K0CE.rXPqcUTjl8ftQWyngnMK ', 'USER', 'ACTIVE'),
        (3, 'user2@mail.ru', 'ivan', 'ivanov', ' $2a$12$sru0m7XHmeFXzFHYnL5cwuumWy1/K0CE.rXPqcUTjl8ftQWyngnMK ', 'USER', 'ACTIVE'),
        (4, 'user3@mail.ru', 'sergey', 'ivanov', '$2a$12$sru0m7XHmeFXzFHYnL5cwuumWy1/K0CE.rXPqcUTjl8ftQWyngnMK', 'USER', 'ACTIVE'),
        (5, 'admin@mail.ru', 'oleg', 'ivanov', '$2a$12$xCW6w/Jbo4BuSvGepWvmVeQuDltPHmPIiHC2R6tD/8IX7c2AmZm/C', 'ADMIN', 'ACTIVE');

INSERT INTO cats (id, name, loise) VALUES
        ('1','musa1', 0),
        ('2','dusa2', 0),
        ('3','husa3', 0),
        ('4','lusa4', 0);