DROP TABLE IF EXISTS cats;
DROP TABLE IF EXISTS users;

CREATE TABLE cats
(
    id     BIGSERIAL    NOT NULL
        CONSTRAINT cats_pk PRIMARY KEY ,
    name   VARCHAR(255) NOT NULL,
    loise INTEGER
);

CREATE UNIQUE INDEX cats_name_uindex ON cats (name);


CREATE TABLE users
(
    id         BIGSERIAL                    NOT NULL
        CONSTRAINT users_pk PRIMARY KEY ,
    email      VARCHAR(255)                 NOT NULL,
    first_name VARCHAR(255)                 NOT NULL,
    last_name  VARCHAR(255)                 NOT NULL,
    password   VARCHAR(255)                 NOT NULL,
    role       VARCHAR(20) DEFAULT 'USER'   NOT NULL,
    status     VARCHAR(20) DEFAULT 'ACTIVE' NOT NULL
);

CREATE UNIQUE INDEX users_email_uindex ON users (email);